package pl.uep.kurs.dzien13;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {

		try {
			Connection polaczenie = BazaDanych.polaczenie();

			String[] dostepneAkcje = new String[] { "koniec", "lista", "dodaj", "usun", "zmien" };

			Scanner skaner = new Scanner(System.in);

			System.out.println("Witamy w wirtualnych meblach. Dostepne akcje:");
			for (String akcja : dostepneAkcje) {
				System.out.println(akcja);
			}

			while (true) {
				System.out.println("Co chcesz zrobi�?");
				String akcjaUzytkownika = skaner.nextLine();

				if (Arrays.asList(dostepneAkcje).contains(akcjaUzytkownika)) {

					if (akcjaUzytkownika.equals("koniec")) {
						System.out.println("Do widzenia :)");
						break;
					} else if (akcjaUzytkownika.equals("lista")) {
						wylistujMeble(polaczenie);
					} else if (akcjaUzytkownika.equals("usun")) {
						System.out.println("Ktory mebel chcesz usunac?");
						wylistujMeble(polaczenie);
						try {
							int idDoUsuniecia = skaner.nextInt();
							usunMebel(polaczenie, idDoUsuniecia);
						} catch (InputMismatchException e) {
							System.out.println("Wprowadzone ID nie jest liczba calkowita!!");
						}
					} else if (akcjaUzytkownika.equals("dodaj")) {
						try {
							System.out.println("Jak nazywa si� nowy mebel:");
							String nazwa = skaner.nextLine();
							System.out.println("Jak� ma mie� cen�:");
							float cena = skaner.nextFloat();
							dodajMebel(polaczenie, nazwa, cena);
							System.out.println(nazwa + " " + cena);
						}

						// przesuwamy linie interpretacji skanera o 1 skaner.nextLine(); }
						catch (InputMismatchException e) {
							System.out.println("Nieprawid�owe dane!!!");
						}
					} else if (akcjaUzytkownika.equals("zmien")) {
						System.out.println("Ktory mebel chcesz zmienic?");
						wylistujMeble(polaczenie);
						System.out.println("Podaj ID");
						try {
							int id = skaner.nextInt();
							System.out.println("Nowa nazwa?");
							skaner.nextLine();
							String nowaNazwa = skaner.nextLine();
							System.out.println("Nowa cena?");
							float nowaCena = skaner.nextFloat();
							skaner.nextLine();
							modyfikujMebel(polaczenie, id, nowaNazwa, nowaCena);
						} catch (InputMismatchException e) {
							System.out.println("Nieprawdlowe parametry. Sprobuje jeszcze raz.");
						}
					}
				} else {
					System.out.println(akcjaUzytkownika);
					if (!akcjaUzytkownika.equals("")) {
						System.out.println("Akcja niedozwolona, sproboj jeszcze raz.");
					}
				}

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static void wylistujMeble(Connection polaczenie) throws SQLException {
		String sql = "SELECT id, nazwa, cena FROM meble;";
		PreparedStatement zapytanie = polaczenie.prepareStatement(sql);

		ResultSet wynik = zapytanie.executeQuery();

		while (wynik.next()) {
			int id = wynik.getInt("id");
			String nazwa = wynik.getString("nazwa");
			float cena = wynik.getFloat("cena");
			System.out.println(id + ". " + nazwa + " " + cena);
		}
	}

	private static void dodajMebel(Connection polaczenie, String nazwa, float cena) throws SQLException {
		String sql = "INSERT INTO meble (nazwa, cena) VALUES (?, ?);";
		PreparedStatement wsad = polaczenie.prepareStatement(sql);
		wsad.setString(1, nazwa);
		wsad.setFloat(2, cena);
		wsad.execute();
	}

	private static void usunMebel(Connection polaczenie, int id) throws SQLException {
		String sql = "DELETE FROM meble WHERE id = ?";
		PreparedStatement wsad = polaczenie.prepareStatement(sql);
		wsad.setInt(1, id);
		wsad.execute();
	}

	private static void modyfikujMebel(Connection polaczenie, int id, String nazwa, float cena) throws SQLException {
		String sql = "UPDATE meble SET nazwa = ?, " + "cena = ? WHERE id = ?";

		PreparedStatement wsad = polaczenie.prepareStatement(sql);
		wsad.setString(1, nazwa);
		wsad.setFloat(2, cena);
		wsad.setInt(3, id);
		wsad.execute();
	}

}

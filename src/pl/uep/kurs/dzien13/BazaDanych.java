package pl.uep.kurs.dzien13;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BazaDanych {

	// moja baza do kt�rej chce si� po��czy�
	static String bazaDanych = "moja_baza";
	// u�ytkownik mysql kt�ry po��czy si� z baz�
	static String uzytkownik = "uzytkownik";
	// has�o u�ytkownika
	static String haslo = "HasloTestowe";
	// tzw. connection string :)
	static String url = "jdbc:mysql://localhost:3306/" + bazaDanych + "?useSSL=false&serverTimezone=UTC&verifyServerCertificate=false&allowPublicKeyRetrieval=true";
	
	// singleto-pattern polaczenie
	private static Connection polaczenie;
	
	public static Connection polaczenie() throws SQLException
	{
		if (polaczenie != null) {
			return polaczenie;
		}
		polaczenie = DriverManager.getConnection(url,uzytkownik,haslo);
		
		return polaczenie;
	}
	
}
